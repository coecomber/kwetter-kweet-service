import { Module } from '@nestjs/common';
import { KweetController } from './kweet.controller';
import { KweetService } from './kweet.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import Kweet from './Kweet/entities/kweet.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: 'kweet_service',
        imports: [ConfigModule.forRoot()],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => {
          return {
            transport: Transport.KAFKA,
            options: {
              client: {
                clientId: 'kweet',
                brokers: configService.get<string>('BROKERS').split(','),
              },
              consumer: {
                groupId: 'kweet-consumer',
              },
            },
          };
        },
      },
    ]),
    
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forFeature([
      Kweet
    ]),
    TypeOrmModule.forRoot(),
  ],
  controllers: [KweetController],
  providers: [KweetService],
})
export class KweetModule {}
