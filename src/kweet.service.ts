import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import Kweet from './Kweet/entities/kweet.entity';

@Injectable()
export class KweetService {
  private readonly logger = new Logger(KweetService.name);

  constructor(
    @InjectRepository(Kweet)
    private kweetRepository: Repository<Kweet>,
  ) { }

  async getAllKweets() {
    return await this.kweetRepository.find(
      {
        order: { kweetCreated: 'DESC' }
      });
  }

  async getOneKweetById(id: string): Promise<Kweet> {
    return await this.kweetRepository.findOne(id);
  }

  async getKweetsOfUserBasedOnPageNumber(ownerId: string, pageNumber: number){
    const take = 10;
    const skip = pageNumber * 10;

    const [result, total] = await this.kweetRepository.findAndCount(
      {
        where:
        { ownerId: ownerId },
        order: { kweetCreated: 'DESC' },
        take: take,
        skip: skip,
      });

    return { data: result, count: total };
  }

  async getAllKweetsBasedOnPageNumber(pageNumber: number) {
    const take = 10;
    const skip = pageNumber * 10;

    const [result, total] = await this.kweetRepository.findAndCount(
      {
        order: { kweetCreated: 'DESC' },
        take: take,
        skip: skip,
      });

    return { data: result, count: total };
  }

  async getFirstTenKweets(pageNumber: number) {
    const take = 10;
    const skip = pageNumber * 10;

    const [result, total] = await this.kweetRepository.findAndCount(
      {
        order: { kweetCreated: 'DESC' },
        take: take,
        skip: skip,
      });

    return { data: result };
  }

  async createKweet(kweet: Kweet): Promise<Kweet> {
    return await this.kweetRepository.save(kweet);
  }
}
