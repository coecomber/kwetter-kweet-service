import * as typeorm from 'typeorm';
import { Transform } from 'class-transformer';

@typeorm.Entity()
export default class Kweet {
    constructor() {
        this.kweetId = '';
        this.kweet = '';
        this.ownerId = '';
        this.kweetCreated = new Date();
        this.kweetUpdated = new Date();
    }

    @typeorm.PrimaryGeneratedColumn('uuid')
    kweetId?: string;

    @typeorm.Column()
    kweet!: string;

    @typeorm.Column()
    ownerId!: string;

    @typeorm.CreateDateColumn()
    kweetCreated?: Date;
  
    @typeorm.UpdateDateColumn()
    kweetUpdated?: Date;
}