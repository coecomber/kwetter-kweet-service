import { Body, Controller, Get, Inject, Logger, Param, Post, Req } from '@nestjs/common';
import { KweetService } from './kweet.service';
import Kweet from './Kweet/entities/kweet.entity';
import { CreateKweet } from './Kweet/dto/kweet.dto';
import { ClientKafka, MessagePattern, Payload } from '@nestjs/microservices';

@Controller('kweet')
export class KweetController {
  constructor(
    @Inject('kweet_service')
    private readonly kafkaProducer: ClientKafka,
    private readonly kweetService: KweetService,
  ) { }

  private readonly logger = new Logger(KweetController.name);

  @Get('/getOne/:id')
  async getOneKweetById(@Param('id') id): Promise<Kweet> {
    this.logger.log('Get called with id:' + id)
    return await this.kweetService.getOneKweetById(id);
  }

  @Get('/kweetsByOwnerId/:ownerId/:pageNumber')
  async getKweetsOfUserBasedOnPageNumber(@Param('ownerId') ownerId: string, @Param('pageNumber') pageNumber: number) {
    this.logger.log('Get kweetsByOwnerId pagenummer called with number:' + pageNumber)
    return this.kweetService.getKweetsOfUserBasedOnPageNumber(ownerId, pageNumber);
  }

  @Get('/kweets/:pageNumber')
  async getAllKweetsBasedOnPageNumber(@Param('pageNumber') pageNumber: number) {
    this.logger.log('Get pagenummer called with number:' + pageNumber)
    return this.kweetService.getAllKweetsBasedOnPageNumber(pageNumber);
  }


  @Get('/tenKweets/')
  async getFirstTenKweets() {
    this.logger.log('Get ten kweets called')
    return this.kweetService.getFirstTenKweets(0).then((res => {
      return res;
    }));
  }

  @Post()
  async createKweet(
    @Body() kweetDto: CreateKweet,
  ): Promise<Kweet> {
    this.logger.log('New kweet created' + kweetDto)
    const kweet: Kweet = { ...kweetDto }
    console.log(kweet)
    return await this.kweetService.createKweet(kweet);
  }


  //#region kafka
  async onModuleInit() {
    this.kafkaProducer.subscribeToResponseOf('testTopic');
    await this.kafkaProducer.connect();
  }

  onModuleDestroy() {
    this.kafkaProducer.close();
  }

  @Get('test')
  test() {
    this.kafkaProducer
    .send('testTopic', "Data")
    .toPromise()
    .catch((error) => this.logger.error(error));
  }

  @MessagePattern("testTopic")
  logTest(@Payload() event: any) {

    console.log(event)
  }
}