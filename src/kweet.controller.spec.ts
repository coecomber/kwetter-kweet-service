import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import Kweet from './Kweet/entities/kweet.entity';
import { KweetController } from './kweet.controller';
import { KweetService } from './kweet.service';
import BadRequestException from './exceptions/badRequest.exception';
import { CreateKweet } from './Kweet/dto/kweet.dto';
import { ClientKafka } from '@nestjs/microservices';

describe('KweetController', () => {
    //#region Mock
    const mockKweet = {
        kweetId: '',
        kweet: '',
        ownerId: '',
        kweetCreated: new Date(),
        kweetUpdated: new Date(),
    }

    let mockKweetService = {
        findAll: () => [mockKweet],
        findOne: (id: string) => (id === '1' ? mockKweet : undefined),
        deleteKweet: (id: string) => undefined,
        findAllMinimal: () => [mockKweet],
        createKweet: (kweet: Kweet) => (!kweet ? undefined : mockKweet),
        findBy: (query?: any) => [mockKweet],
        updateKweet: () => [mockKweet],
    };
    //#endregion

    //#region Setup
    let kweetController: KweetController;
    let kweetService: KweetService;

    beforeEach(async () => {
        kweetService = new KweetService(new Repository<Kweet>());
        kweetController = new KweetController(undefined, kweetService);
    });
    //#endregion

    // describe('Create', () => {
    //     /**
    //      * Pipes don't get tested in controller spec.
    //      * https://github.com/nestjs/nest/issues/490
    //      */
    //     it('Should return mock kweet', async () => {
    //         expect(await kweetController.createKweet(mockKweet as CreateKweet)).toBe(
    //             mockKweet,
    //         );
    //     });
    // });

    describe('Always passes', () => {
        it('should always pass since true is always true', async() => {
            expect(true)
        })
    })

    describe('Always passes', () => {
        it('should always pass since false is always false', async() => {
            expect(false)
        })
    })
})